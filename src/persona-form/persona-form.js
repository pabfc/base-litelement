import {LitElement, html} from 'lit-element';

class PersonaForm extends LitElement {

    static get properties() {
        return {
            person: {type: Object},
            editingPerson: {type: Boolean}
        }; 
    }

    constructor () {
        super();               
        this.resetFormData();
        this.editingPerson = false;
    }

    render() {

        //El value del input debe ser .value para poder ser tratado como propiedad
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <div>
                <form>
                    <div class="form-group">
                        <label>Nombre completo</label>
                        <input type="text" 
                        @input=${this.updateName} 
                        .value="${this.person.name}" 
                        ?disabled="${this.editingPerson}"
                        class="form-control" placeholder="Nombre completo" />                                                
                    </div>
                    <div class="form-group">
                        <label>Perfil</label>
                        <textarea @input=${this.updateProfile} .value="${this.person.profile}" class="form-control" placeholder="Perfil" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Años en la empresa</label>
                        <input type="text" @input=${this.updateYearsInCompany} .value="${this.person.yearsInCompany}" class="form-control" placeholder="Años en la empresa" />
                    </div>
                    <button class="btn btn-primary" @click=${this.goBack}>Atrás</button>
                    <button class="btn btn-success" @click=${this.storePerson}>Guardar</button>
                </form>
            </div>
            
        `;
    }    

    updateName (e) {
        console.log("updateName");
        console.log("Actualizando la propiedad name de person con el valor " + e.target.value);
        this.person.name = e.target.value;
    }

    updateProfile (e) {
        console.log("updateProfile");
        console.log("Actualizando la propiedad profile de person con el valor " + e.target.value);
        this.person.profile = e.target.value;
    }

    updateYearsInCompany (e) {
        console.log("updateYearsInCompany");
        console.log("Actualizando la propiedad yearsInCompany de person con el valor " + e.target.value);
        this.person.yearsInCompany = e.target.value;
    }

    goBack (e) {
        console.log("goBack");
        e.preventDefault();
        this.resetFormData(); //Borramos los campos del formulario al ir atrás
        this.dispatchEvent(new CustomEvent("persona-form-close", {}));
    }

    storePerson(e) {
        console.log("storePerson");
        e.preventDefault();
        this.person.photo = {
            src: './img/frodo.jpg',
            alt:  'Persona'
        };
        console.log("Las propiedades name en person vale " + this.person.name);
        console.log("Las propiedades profile en person vale " + this.person.profile);
        console.log("Las propiedades years en person vale " + this.person.yearsInCompany);

        this.dispatchEvent(new CustomEvent("persona-form-store", 
            { detail: { 
                person: { 
                    name : this.person.name,
                    profile: this.person.profile,
                    yearsInCompany: this.person.yearsInCompany,
                    photo: this.person.photo                
                }, 
                editingPerson: this.editingPerson
            }}
        ));
        
    }

    resetFormData () {
        console.log("resetFormData");
        this.person = { 
            name : "", 
            profile : "", 
            yearsInCompany : ""
        };
    }
}

customElements.define('persona-form', PersonaForm) 