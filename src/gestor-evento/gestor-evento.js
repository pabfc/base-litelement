import {LitElement, html} from 'lit-element';
import '../emisor-evento/emisor-evento.js';
import '../receptor-evento/receptor-evento.js';
class GestorEvento extends LitElement {

    static get properties() {
        return {}; 
    }

    constructor () {
        super();
    }

    render() {
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <h1>Gestor Evento</h1>
            <emisor-evento @test-event="${this.processEvent}" ></emisor-evento>
            <receptor-evento id="receiver"></receptor-evento>

        `;
    }

    processEvent (e) {
        console.log("processEvent");
        console.log(e);       
        this.shadowRoot.getElementById("receiver").course = e.detail.course; //this.shadowroot es el dom del componente
        this.shadowRoot.getElementById("receiver").year = e.detail.year; //this.shadowroot es el dom del componente        
        //var receptorEvento = this.shadowRoot.getElementById("receiver"); //this.shadowroot es el dom del componente
        //receptorEvento.course = e.detail.course;
        //receptorEvento.year = e.detail.year;
    }

}

customElements.define('gestor-evento', GestorEvento) 