import {LitElement, html} from 'lit-element';

class PersonaSidebar extends LitElement {

    static get properties() {
        return {
            peopleStats: {type: Object}
        }; 
    }

    constructor () {
        super();
        this.peopleStats = {};
    }

    render() {
        return html `
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <aside>
            <section class="mt-5">
            <div>
                Hay <span class="badge bg-pill bg-primary">${this.peopleStats.numberOfPeople}</span> personas
            </div>
                <div>
                    <button class="w-100 btn btn-success" style="font-size: 50px" @click=${this.newPerson}>
                        <strong>+</strong>
                    </button>
                </div>
            </section>
        </aside>
        `;
    }    

    updated (changedProperties) {
        console.log("updated en persona-sidebar");
        if (changedProperties.has("peopleStats")) {
            console.log("peopleStats ha cambiado en persona-sidebar");
            console.log(this.peopleStats);
        }
    }

    newPerson() {
        console.log("new person en sidebar");
        console.log("Se va a crear una nueva persona");
        this.dispatchEvent(new CustomEvent("new-person",  {}));

    }
}

customElements.define('persona-sidebar', PersonaSidebar) 