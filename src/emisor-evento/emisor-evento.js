import {LitElement, html} from 'lit-element';

class EmisorEvento extends LitElement {

    static get properties() {
        return {}; 
    }

    constructor () {
        super();
    }

    render() {
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <h3>Emisor Evento</h3>
            <button class="btn btn-primary" @click="${this.sendEvent}">No pulsar</button>

        `;
    }    

    sendEvent (e) {
        console.log("Pulsado el botón");
        console.log(e);
        this.dispatchEvent(
            new CustomEvent(
                "test-event", // siempre minúsculas!
                {
                    "detail":  {
                        "course": "TechU",
                        "year": 2021
                    } 
                },
            )
        )
    }
}

customElements.define('emisor-evento', EmisorEvento) 