import {LitElement, html} from 'lit-element';

class ReceptorEvento extends LitElement {

    static get properties() {
        return {
            course: {type: String},
            year: {type:String}
        }; 
    }

    constructor () {
        super();
    }

    render() {
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <h3>Receptor Evento</h3>         
            <h5>Este curso es de ${this.course}</h5>
            <h5>y estamos en el año ${this.year}</h5>

        `;
    }    

}

customElements.define('receptor-evento', ReceptorEvento) 