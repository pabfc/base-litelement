import {LitElement, html} from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';

class PersonaMain extends LitElement {

    static get properties() {
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean}
        }; 
    }


    constructor () {
        super();        
        this.people = [
            { name: "Frodo", yearsInCompany: 10, profile: "Lorem ipsum dolor sit amet.", photo: { src: "./img/frodo.jpg", alt: "Frodo Bolsón" }} , 
            { name: "Bilbo", yearsInCompany: 30, profile: "Lorem ipsum.", photo: { src: "./img/bilbo.png", alt: "Bilbo Bolsón"}} , 
            { name: "Merry", yearsInCompany: 6, profile: "ipsum dolor.", photo: { src: "./img/merry.jpg", alt: "Merry" }},
            { name: "Leela", yearsInCompany: 6, profile: "sit amet.", photo: { src: "./img/merry.jpg", alt: "Leela" }},
            { name: "Tyrion", yearsInCompany: 6, profile: "amet sit dolor.", photo: { src: "./img/merry.jpg", alt: "Tyrion" }}
        ];
        this.showPersonForm = false;
    }

    render() {        
                    
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4" >
                ${this.people.map(
                    person => html`
                    <persona-ficha-listado 
                        fname="${person.name}" 
                        yearsInCompany="${person.yearsInCompany}"
                        profile="${person.profile}"
                        .photo="${person.photo}"
                        @delete-person="${this.deletePerson}"
                        @info-person="${this.infoPerson}"
                    >
                    </persona-ficha-listado>
                    `)}
                </div>
            </div>    
            <div class="row">    
                <persona-form id="person-form" class="d-none border rounded border-primary" 
                    @persona-form-close=${this.personFormClose} 
                    @persona-form-store=${this.personFormStore}>
                </persona-form>
            </div>
            
        `;
    }    

    updated (changedProperties) {
        console.log("updated");
        if (changedProperties.has("showPersonForm")) {
            console.log("Ha cambiado showPersonForm en persona-main");
            if (this.showPersonForm === true) {
                this.showPersonFormData();    
            } else {
                this.showPersonList();
            }
        }

        if(changedProperties.has("people")) {
            console.log("Ha cambiado el valor de people en persona-main");
            this.dispatchEvent(
                new CustomEvent("updated-people", {
                    "detail": {
                        people: this.people
                    }

                })
            );
        }
    }

    showPersonFormData () {
        console.log("showPersonFormData");
        console.log("Mostrando formulario de nueva persona");
        this.shadowRoot.getElementById("person-form").classList.remove("d-none");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
    }

    showPersonList () {
        console.log("showPersonList");
        console.log("Mostrando lista de personas");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("person-form").classList.add("d-none");        
    }

    personFormClose () {
        console.log("Se ha cerrado el formulario de la persona");
        this.showPersonForm = false;
    }

    personFormStore (e) {
        console.log("personFormStore");
        console.log("Se va a almacenar una persona");

        console.log("La propiedad name de person vale: " + e.detail.person.name);
        console.log("La propiedad profile de person vale: " + e.detail.person.profile);
        console.log("La propiedad yearsInCompany de person vale: " + e.detail.person.yearsInCompany);
        
        if (e.detail.editingPerson === true) {
            console.log("Se va a actualizar la persona de nombre " + e.detail.person.name);
            
            //Forzamos que se modifique para que la actualización de people llame al updated
            this.people = this.people.map( 
                person => person.name === e.detail.person.name ? person = e.detail.person : person);

            /*
            let indexOfPerson = this.people.findIndex ( person => person.name === e.detail.person.name);
            if ( indexOfPerson >= 0 ) {
                console.log("Persona encontrada");
                this.people[indexOfPerson] = e.detail.person;
            }*/
        } 
        else {
            console.log("Se va a almacenar una persona nueva");
            //this.people.push(e.detail.person);
            //Forzamos que se modifique people para que se llame al updated
            this.people = [...this.people, e.detail.person];
        
        }
        console.log("Persona almacenada");
        this.showPersonForm = false;

    }

    deletePerson (e) {
        console.log("deletePerson en persona-main");
        console.log("Se va a borrar la persona  de nombre " + e.detail.name);
        this.people = this.people.filter( 
            person => person.name != e.detail.name);        
    }

    infoPerson (e) {
        console.log("infoPerson en persona-main");
        console.log("Se ha pedido más información de la persona de nombre " + e.detail.name);
        let chosenPerson = this.people.filter(person => person.name === e.detail.name);
        console.log(chosenPerson);


        let personShow = {};
        personShow.name = chosenPerson[0].name;
        personShow.yearsInCompany = chosenPerson[0].yearsInCompany;
        personShow.profile = chosenPerson[0].profile;

        //No podemos poner chosenPerson[0] porque cogería la referencia y se actaulizaría aunque clicáramos atrás
        //this.shadowRoot.getElementById("person-form").person = chosenPerson[0];
        
        this.shadowRoot.getElementById("person-form").person = personShow;
        this.shadowRoot.getElementById("person-form").editingPerson = true;
        console.log("Ha cambiado el editingPerson. Nuevo valor: " + this.shadowRoot.getElementById("person-form").editingPerson);
        this.showPersonForm = true;        

    }
}

    customElements.define('persona-main', PersonaMain) 